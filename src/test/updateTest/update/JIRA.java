package test.updateTest.update;


import test.config.Log;
import test.config.Parameters;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Base64;

/**
 * Created by d.fedorov on 22.01.2018.
 */
public class JIRA {

    private final  String JIRA_URI = "https://crystals.atlassian.net";
    private final  String JIRA_LOGIN = Parameters.system.get("jira.login");
    private final  String JIRA_PASSWORD = Parameters.system.get("jira.pwd");
    private final  String RESULT = "customfield_11201";
    private final  String SQL = "customfield_11200";


    //Получаем файлы, добавляемые в сборку

    public ArrayList <String>  startUpdate() throws IOException {

        final URI jiraServerUri = URI.create(JIRA_URI);
        final AsynchronousJiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
        JiraRestClient restClient = factory.createWithBasicHttpAuthentication(jiraServerUri,JIRA_LOGIN,JIRA_PASSWORD);
        return handleProject(restClient);
    }


    private ArrayList <String>  handleProject(JiraRestClient restClient) {
        FTPSET5 ftp = new FTPSET5();
        ftp.ftpLogin();
        ftp.downloadSet5();
        ftp.createVersion(restClient);
        ftp.ftpClose();
        return  FTPSET5.getError();
    }


    public ArrayList<String> getIssueResult(Issue issue) {

        ArrayList<String> issuesResult = new ArrayList<>();
        issuesResult.add(0,"");
        issuesResult.add(1,"");

        try {

            DefaultHttpClient httpclient = new DefaultHttpClient();

            String encoding = Base64.getEncoder().encodeToString((JIRA_LOGIN + ":" + JIRA_PASSWORD).getBytes());
            HttpGet httppost = new HttpGet(issue.getSelf().toString());
            httppost.setHeader("Authorization", "Basic " + encoding);

            System.out.println("executing request " + httppost.getRequestLine());
            HttpResponse response = httpclient.execute(httppost);
            String json = EntityUtils.toString(response.getEntity());

            Gson gson = new Gson();
            JsonElement element = gson.fromJson(json, JsonElement.class);
            JsonObject jsonObj = element.getAsJsonObject();

            JsonElement resultElement = jsonObj.get("fields").getAsJsonObject().get(RESULT);
            JsonElement scriptsElement = jsonObj.get("fields").getAsJsonObject().get(SQL);

            if (resultElement != JsonNull.INSTANCE) {
                Log.logger.debug("Issue " +issue.getKey()+" result " + resultElement);
                issuesResult.add(0, resultElement.getAsString());
            }

            if (scriptsElement != JsonNull.INSTANCE) {
                Log.logger.debug("Issue " +issue.getKey()+" SQL scripts " + scriptsElement);
                issuesResult.add(1, scriptsElement.getAsString());
            }
        }

        catch (IOException e) {
            FTPSET5.setError("Ошибка."+"Задача "+ issue.getKey()+ ".Ошибка в получении поля sql или Result!");
            e.printStackTrace();
        }

        return issuesResult;
    }
}
