package test.updateTest.update;

import org.testng.Assert;
import test.config.Parameters;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by d.fedorov on 22.01.2018.
 */
public class Update {

    private final String SEVENZ_TO = Parameters.system.get("7z.toLocal");
    private final String FTP_TEMPLATE = Parameters.system.get("ftp.templatePath").replace("%version%", Parameters.system.get("VersionTo"));
    private final String FTP_COPY = Parameters.system.get("ftp.toCopy");
    private final Integer GET_UPDATE = Integer.valueOf(Parameters.system.get("update.getUpdate"));
    private final Integer UPDATE_UI_COMPRESS = Integer.valueOf(Parameters.system.get("update.updateAndCompress"));
    private final Integer PUSH_FTP = Integer.valueOf(Parameters.system.get("update.pushFTP"));
    private final Integer DISTRIB = Integer.valueOf(Parameters.system.get("update.Distrib"));

    public void start() throws IOException,InterruptedException,AWTException {

        JIRA jira = new JIRA();
        ArrayList<String> errors = new ArrayList<>();

        if (GET_UPDATE==1) {
            errors = jira.startUpdate();
            if (errors.size() != 0) {
                Assert.fail();
            }
        }

        if (UPDATE_UI_COMPRESS==1) {
            UpdateUI updateUI = new UpdateUI();
            updateUI.start();
            SevenZ.compress(SEVENZ_TO, new File(FTP_COPY + FTP_TEMPLATE+"\\Output").listFiles());
        }

        if (PUSH_FTP==1) {
            FTPSET5 ftp = new FTPSET5();
            ftp.pushToFtp();
        }

        if (DISTRIB==1) {
            DistribUpdate distrib  = new DistribUpdate();
            distrib.runClientInstall();
            distrib.runServerInstall();
        }
    }
}
