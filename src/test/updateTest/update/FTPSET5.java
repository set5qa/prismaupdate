package test.updateTest.update;

import test.config.Log;
import test.config.Parameters;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.BasicComponent;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.SearchResult;
import com.google.common.collect.Iterables;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.testng.Assert;

import java.io.*;
import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by d.fedorov on 23.01.2018.
 */
public class FTPSET5 {

    private final String SERVER = Parameters.system.get("ftp.server");
    private final String USER = Parameters.system.get("ftp.login");
    private final String PASS = Parameters.system.get("ftp.password");
    private final String FTP_TEMPLATE= Parameters.system.get("ftp.templatePath").replace("%version%", Parameters.system.get("VersionTo"));
    private final String FTP_TEAMCITY = Parameters.system.get("ftp.teamcity").replace("%version%", Parameters.system.get("VersionTo"));
    private final String FTP_COPY = Parameters.system.get("ftp.toCopy");
    private final String FTP_HANDS = Parameters.system.get("ftp.hands").replace("%version%", Parameters.system.get("VersionTo"));
    private final String JQL_QUERY= Parameters.system.get("jira.jqlSP");
    private final String VERSION = Parameters.system.get("VersionTo");
    private final String SEVENZ_TO = Parameters.system.get("7z.toLocal");
    private final String SQL_SCRIPT = "pgdb Video:";
    private final String HANDS_DOC ="Documentation PrismaR:";


    private final static FTPClient client = new FTPClient();
    private final static Set<String> fields = Stream.of("summary","details","fixVersions","resolution","description", "issuetype","components","created", "updated", "project", "status", "key").collect(Collectors.toSet());
    private final static int maxResults = 500;


    private static  ArrayList <String> ERROR = new ArrayList<>();


    public void ftpLogin() {

        try {
            client.setControlEncoding("UTF-8");
            client.connect(SERVER);
            int replyCode = client.getReplyCode();

            if (!FTPReply.isPositiveCompletion(replyCode)) {
                setError("Ошибка.Подключение к FTP провалилось с кодом: " + replyCode);
                Assert.fail();
            }

            client.enterLocalPassiveMode();
            boolean success = client.login(USER, PASS);

            if (!success) {
                setError("Ошибка.Неверный логин/пароль подключения к FTP!");
                Assert.fail();
            }
        }

        catch (IOException ex) {
            ex.printStackTrace();
            setError("Ошибка. FTP. Не могу подключиться!");
            Assert.fail();
        }

    }

    public void ftpClose() {
        try {
            client.disconnect();
        }

        catch (IOException e) {
            e.printStackTrace();
            setError("Ошибка. FTP. Не могу отключиться!");
            Assert.fail();
        }
    }


    public void createVersion(JiraRestClient restClient) {

        int startAt = 0;
        String jql = JQL_QUERY;

        jql = jql.replace("%version%", VERSION);
        Log.logger.debug("Executing jira jql: " + jql);
        SearchResult searchJqlPromise = restClient.getSearchClient().searchJql(jql, maxResults, startAt, fields).claim();

        //проверяю на наличие каких-либо задачек
        if (searchJqlPromise.getTotal() == 0) {
            setError("Ошибка! Нет задач в статусе 'Testing','Resolved','Ready for Test', or 'In Progress' в версии!");
            Assert.fail();
        }

        //check for all issues
        ftpCreateReadme(checkAllIssues(searchJqlPromise));
        //finalize Update
        finalizeUpdate();
    }


    public void downloadSet5() {

        File newDir = new File(FTP_COPY+ getBeforeSlash(FTP_TEMPLATE));
        boolean created = newDir.mkdirs();
        if (created) {
            Log.logger.debug("Создана директория: " + newDir);
        } else {
            setError("Не могу создать директорию: " + newDir);
            Assert.fail();
        }

        try {
             downloadDirectory(null,client,getBeforeSlash(FTP_TEMPLATE),getAfterLastSlash(FTP_TEMPLATE), FTP_COPY);
             downloadDirectory(null,client,getBeforeSlash(FTP_HANDS),getAfterLastSlash(FTP_HANDS), FTP_COPY);
        }

        catch (IOException e) {
            setError("Ошибка загрузки шаблона: ");
            e.printStackTrace();
            Assert.fail();
        }
    }


    public void ftpCheckComponentAndCopy(BasicComponent co,Issue issue) {

        ArrayList <String> sqlScript = new ArrayList();
        if (Parameters.jiraComponents.get(co.getName()) != null) {
            try {
                File file = new File (FTP_COPY+FTP_TEAMCITY+Parameters.jiraComponents.get(co.getName()));
                file.mkdirs();

                //скрипт содержит компонент
                if (SQL_SCRIPT.contains(co.getName())||HANDS_DOC.contains(co.getName())) {

                    JIRA jira = new JIRA();
                    String sql=jira.getIssueResult(issue).get(1);

                    if (sql.contains(co.getName())) {
                        String [] sqlarray;
                        sqlarray=sql.split("\r\n");

                        if (sqlarray.length<2) {
                            setError("Ошибка.Работа " +issue.getKey()+"Нет скриптов в этой работе");
                        }

                        for (int i=0;i<sqlarray.length;i++) {

                            if ((sqlarray[i]).equals(SQL_SCRIPT) && co.getName().equals(SQL_SCRIPT.replace(":","")) ) {

                                while (i+1<sqlarray.length && !sqlarray[i + 1].contains(":")) {
                                    File sqlFile = new File(FTP_COPY + FTP_TEAMCITY + Parameters.jiraComponents.get(co.getName()) + sqlarray[i + 1]);
                                    downloadSingleFile(issue, client, FTP_TEAMCITY + Parameters.jiraComponents.get(co.getName()) + sqlarray[i + 1], sqlFile.getPath());
                                    i++;
                                }
                            }

                            if ((sqlarray[i]).equals(HANDS_DOC) && co.getName().equals(HANDS_DOC.replace(":",""))) {

                                while (i+1<sqlarray.length && !sqlarray[i + 1].contains(":")) {
                                    File docFile = new File(FTP_COPY + FTP_HANDS + Parameters.jiraComponents.get(co.getName()) + sqlarray[i + 1]);
                                    if (!docFile.exists()) {
                                        setError("Ошибка.FTP.Работа " + issue.getKey() + "Файл " + docFile + " не найден");
                                    }
                                    i++;
                                }
                            }
                        }
                    }
                    else {
                        setError("Ошибка.Работа " +issue.getKey()+ ".Отсутствует компонент " +co.getName() + " в поле SQL скрипт");

                    }
                }

                else {
                    downloadDirectory(issue,client, FTP_TEAMCITY, Parameters.jiraComponents.get(co.getName()), FTP_COPY);
                }
            }

            catch (Exception e) {
                setError("FTP.Работа "+issue.getKey()+"Ошибка загрузки компонента " + co.getName());
                e.printStackTrace();
                Assert.fail();
            }
        }

       else {
            setError("FTP.Проверьте файл _UDF_JiraCrystals_Component.txt на наличие компонента " + co.getName());
            Assert.fail();
        }
    }


    private ArrayList <Issue> checkAllIssues(SearchResult searchJqlPromise) {

        ArrayList<String> usedComponents = new ArrayList<>();
        ArrayList<Issue> allIssues = new ArrayList<>();

        for (Issue issue : searchJqlPromise.getIssues()) {
            Iterable<BasicComponent> components = issue.getComponents();
            //ignore subtask

            if (!issue.getIssueType().isSubtask()) {
                //check component in every issue
                if (Iterables.size(components)!=0) {
                    for (BasicComponent bc : components) {
                        // check corresponding File on FTPSET5
                        Log.logger.debug("Получил компонент " + bc.getName() + "из задачи " + issue.getKey());

                        if (!usedComponents.contains(bc.getName())) {
                            ftpCheckComponentAndCopy(bc,issue);
                            //если не содержит - не возвращаемся
                            if (!SQL_SCRIPT.contains(bc.getName())) {
                                usedComponents.add(bc.getName());
                            }
                        }

                        else {
                            Log.logger.debug("Компонент уже был загружен");
                        }
                    }
                }
                else {
                    setError("Ошибка.FTP.Работа " + issue.getKey() + " Не имеет компонентов");
                }
                allIssues.add(issue);
            }
        }
        return allIssues;
    }


    private void finalizeUpdate(){

        //Переименование директории
        File srcDir = new File(FTP_COPY+FTP_TEAMCITY);
        File srcToDel = new File(FTP_COPY+getBeforeSlash(FTP_TEAMCITY));

        File srcDirHands = new File(FTP_COPY+FTP_HANDS);
        File destDir = new File(FTP_COPY+ FTP_TEMPLATE +"/UpGrade/"+VERSION);

        try {

            if (srcDirHands.exists()) {
                FileUtils.copyDirectory(srcDirHands, destDir, true);
                FileUtils.forceDelete(srcDirHands);
            }

            FileUtils.copyDirectory(srcDir, destDir,true);
            FileUtils.forceDelete(srcToDel);
        }

        catch (IOException e) {
            setError("Ошибка при копировании компонентов! ");
            e.printStackTrace();
            Assert.fail();
        }
    }


    /**
     * Download a single file from the FTP server
     * @param ftpClient an instance of org.apache.commons.net.ftp.FTPClient class.
         * @param remoteFilePath path of the file on the server
         * @param savePath path of directory where the file will be stored
         * @return true if the file was downloaded successfully, false otherwise
         * @throws IOException if any network or IO error occurred.
         */

    private boolean downloadSingleFile(Issue issue,FTPClient ftpClient, String remoteFilePath, String savePath) throws IOException {
        File downloadFile = new File(savePath);

        File parentDir = downloadFile.getParentFile();

        if (!parentDir.exists()) {
            parentDir.mkdir();
        }

        FTPFile[] remoteFiles = ftpClient.listFiles(remoteFilePath);

        if (remoteFiles.length == 0) {
            setError("Ошибка.FTP.Работа " + issue.getKey() + ". Файл "+ remoteFilePath +" не найден");
        }

        OutputStream outputStream = new BufferedOutputStream(
                new FileOutputStream(downloadFile));
        try {
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            return ftpClient.retrieveFile(remoteFilePath, outputStream);
        } catch (IOException ex) {
            setError("Ошибка.FTP.Работа " + issue.getKey() + "Файл "+ remoteFilePath +" не может быть загружен");
            throw ex;
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }


    private void ftpCreateReadme(ArrayList <Issue> issues) {

        BufferedWriter writer = null;

        //create README
        File files = new File(FTP_COPY+FTP_TEMPLATE+"/UpGrade/readme"+VERSION+"_PrismaR.txt");

        files.getParentFile().mkdirs();


        ArrayList <Issue> bug = new ArrayList<>();
        ArrayList <Issue> others = new ArrayList<>();

        try {

            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(files), "utf-8"));
            files.createNewFile();
            writer.write("***************************************************************************************************");
            addNewLinesInFile(writer,2);
            writer.write("версия "+Parameters.system.get("VersionTo") + " PrismaR");
            addNewLinesInFile(writer,2);
            writer.write("Доработки: ");
            addNewLinesInFile(writer,2);

            for (int i=0;i<issues.size();i++) {
               if (issues.get(i).getIssueType().getName().equals("Bug")) {
                    bug.add(issues.get(i));
                }
                else {
                    others.add(issues.get(i));
               }
            }

            createHalfRead(others,writer);

            if (bug.size()!=0) {

                addNewLinesInFile(writer, 2);
                writer.write("------------------------");
                addNewLinesInFile(writer, 2);
                writer.write("Исправленные ошибки: ");
                addNewLinesInFile(writer, 2);
                createHalfRead(bug, writer);
            }
                writer.close();
        }

        catch (IOException e) {
            setError("Ридми файл не может быть создан!");
            e.printStackTrace();
            Assert.fail();
        }

    }

    private void createHalfRead(ArrayList <Issue> others,BufferedWriter writer) throws IOException {

        for (int i = 0; i < others.size(); i++) {

            //ignore subtasks
            if (!others.get(i).getIssueType().isSubtask()) {

                writer.newLine();
                writer.write(String.valueOf(i + 1) + ".       " + others.get(i).getKey());

                if (!others.get(i).getStatus().getName().equals("Resolved")) {
                    writer.write(" РАБОТА НЕ ЗАКРЫТА. СТАТУС:  " + others.get(i).getStatus().getName());
                }

                addNewLinesInFile(writer, 1);

                if (others.get(i).getSummary() != null) {
                    writer.write("           " + others.get(i).getSummary());
                }

                addNewLinesInFile(writer, 1);
                writer.write("           " + "Компоненты:");
                addNewLinesInFile(writer, 1);

                if (others.get(i).getComponents() != null) {
                    for (BasicComponent bc : others.get(i).getComponents()) {
                        writer.write("           " + bc.getName());
                        addNewLinesInFile(writer, 1);
                    }
                }

                JIRA jira = new JIRA();
                String result = jira.getIssueResult(others.get(i)).get(0);

                if (!result.equals("")) {
                    writer.write("           " + "Описание работы: ");
                    addNewLinesInFile(writer, 1);
                    writer.write("           "+result);
                    addNewLinesInFile(writer, 1);
                }

                 String sql = jira.getIssueResult(others.get(i)).get(1);

                 if (!sql.equals("")) {
                     writer.write("           " + "Скрипты/Документация/Шаблоны: ");
                     String[] sqlarray;
                     sqlarray = sql.split("\r\n");
                     for (String str : sqlarray) {
                         addNewLinesInFile(writer,1);
                         writer.write("           "+str);
                        }
                    }
                 }
            }
        }


    private void addNewLinesInFile (BufferedWriter writer,int num) {

        for (int i=0;i<num;i++) {
            try {
                writer.newLine();
            }

            catch (IOException e) {
                setError("Ридми файл не может быть создан!");
                e.printStackTrace();
                Assert.fail();
            }
        }
    }

    private String getBeforeSlash(String current) {
        current = current.substring(0,current.indexOf("/"));
        return  current;
    }

    private String getAfterLastSlash(String current) {

        current = current.substring(current.lastIndexOf("/"),current.length());
        return current;
    }


    /**
     * Download a whole directory from a FTP server.
     * @param ftpClient an instance of org.apache.commons.net.ftp.FTPClient class.
     * @param parentDir Path of the parent directory of the current directory being
     * downloaded.
     * @param currentDir Path of the current directory being downloaded.
     * @param saveDir path of directory where the whole remote directory will be
     * downloaded and saved.
     * @throws IOException if any network or IO error occurred.
     */

    private void downloadDirectory(Issue issue,FTPClient ftpClient, String parentDir, String currentDir, String saveDir) throws IOException {


        String dirToList = parentDir;
        if (!currentDir.equals("")) {
            dirToList += "/" + currentDir;
        }

        FTPFile[] subFiles = ftpClient.listFiles(dirToList);



        if (subFiles != null && subFiles.length > 0) {
            for (FTPFile aFile : subFiles) {
                String currentFileName = aFile.getName();
                if (currentFileName.equals(".") || currentFileName.equals("..")) {
                    // skip parent directory and the directory itself
                    continue;
                }
                String filePath = parentDir + "/" + currentDir + "/"
                        + currentFileName;
                if (currentDir.equals("")) {
                    filePath = parentDir + "/" + currentFileName;
                }

                String newDirPath = saveDir + parentDir + File.separator
                        + currentDir + File.separator + currentFileName;
                if (currentDir.equals("")) {
                    newDirPath = saveDir + parentDir + File.separator
                            + currentFileName;
                }

                if (aFile.isDirectory()) {
                    // create the directory in saveDir
                    File newDir = new File(newDirPath);
                    boolean created = newDir.mkdirs();

                    if (created) {
                        Log.logger.debug("Created directory: " + newDirPath);
                    }
                    else {
                        Log.logger.debug("Could not create directory: " + newDirPath);
                    }

                    // download the sub directory
                    downloadDirectory(issue,ftpClient, dirToList, currentFileName,saveDir);
                } else {
                    // download the file
                    boolean success = downloadSingleFile(issue,ftpClient, filePath,
                            newDirPath);
                    if (success) {
                        Log.logger.debug("Downloaded file: " + filePath);
                    } else {
                        setError("Ошибка.Не возможно загрузить файл " + filePath);
                    }
                }
            }
        }

        else {
            setError("Ошибка.FTP.Работа " + issue.getKey() + ".Директория "+ dirToList +" пустая или не существует");
        }
    }

    public void pushToFtp()  {

        try {
            ftpLogin();
            FileInputStream fis = new FileInputStream(SEVENZ_TO);
            client.changeWorkingDirectory("Test_builds");
            client.setFileType(FTP.BINARY_FILE_TYPE);
            client.storeFile(SEVENZ_TO.substring(SEVENZ_TO.lastIndexOf("\\"),SEVENZ_TO.length()), fis);
            fis.close();
        }

        catch (IOException e) {
            e.printStackTrace();
            setError("Ошибка."+" Невозможно выложить сборку на FTP");
            Assert.fail();
        }
        ftpClose();
    }


    public static ArrayList<String> getError() {
       return ERROR;
    }

    public static void setError(String str){
        if (!ERROR.contains(str)) {
            ERROR.add(str);
        }
    }
}

