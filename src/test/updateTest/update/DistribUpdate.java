package test.updateTest.update;

import autoitx4java.AutoItX;
import com.jacob.com.LibraryLoader;
import org.testng.Assert;
import test.config.Log;
import test.config.Parameters;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by d.fedorov on 21.03.2019.
 */
public class DistribUpdate {
    private final String START_DISTRIB = Parameters.system.get("distrib.Path").replace("%version%", Parameters.system.get("VersionTo"));

    private  AutoItX X = new AutoItX();
    private final String AGENT_HOST_PORT=Parameters.system.get("distrib.AgentIP");
    private final String CLIENT_POSTGRES_IP = Parameters.system.get("distrib.ClientPlaceIP");
    private String INSTALL_DIR="C:/Prisma2";

    private int  SLEEP_TIME=1000;
    private final String PASSWORD="postgres";
    //forms
    private final String START_FORM =  "[CLASS:TWizardForm]";
    private final String TITLE_START_WINDOW = "Установка — Set Prisma, версия " + Parameters.system.get("VersionTo");
    private final String TITLE_INSTALL_SQL = "Установка";

    //controls;
    private final String RADIO_BUTTON_ACCEPT_LICENSE="[CLASS:TNewRadioButton; INSTANCE:1]";
    private final String NEXT_START_WINDOW="[CLASS:TNewButton; INSTANCE:1]";
    private final String NEXT="[CLASS:TNewButton; INSTANCE:3]";
    private final String EDIT_LICENSE = "[CLASS:TPasswordEdit; INSTANCE:1]";
    private final String EDIT_DIRECTORY = "[CLASS:TEdit; INSTANCE:1]";
    private final String CHECK_BOX = "[CLASS:TNewCheckListBox; INSTANCE:1]";
    private final String PASSWORD_EDIT_POSTGRES="[CLASS:TPasswordEdit; INSTANCE:2]";
    private final String PASSWORD_EDIT_POSTGRES_CONFIRM="[CLASS:TPasswordEdit; INSTANCE:3]";
    private final String POSTGRES_PORT="[CLASS:TPasswordEdit; INSTANCE:4]";
    private final String FINAL_INSTALL_TEXT="[CLASS:TNewMemo; INSTANCE:1]";
    private final String OK_BUTTON = "[CLASS:Button; INSTANCE:1]";




    public void runServerInstall() throws IOException,InterruptedException,AWTException {

        String jacobDllVersionToUse = "jacob-1.18-x64.dll";
        File file = new File(jacobDllVersionToUse);

        System.setProperty("jna.library.path", file.getAbsolutePath());
        System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());

        Robot sendKey = new Robot();
        RunBatScript(START_DISTRIB, true);
        Log.logger.debug(START_DISTRIB);


        System.out.print(X.winExists(START_FORM));

        //License Window
        waitWindowExists(START_FORM,SLEEP_TIME,SLEEP_TIME*5);
        controlClick(RADIO_BUTTON_ACCEPT_LICENSE);

        controlClick(NEXT_START_WINDOW);
        waitControlExists(TITLE_START_WINDOW,EDIT_DIRECTORY, SLEEP_TIME, SLEEP_TIME * 5);
        X.controlSend(TITLE_START_WINDOW, "", EDIT_DIRECTORY, INSTALL_DIR);


        //Directory window
        controlClick(NEXT);
        waitControlExists(TITLE_START_WINDOW,CHECK_BOX, SLEEP_TIME, SLEEP_TIME * 5);

        //client - server window
        controlClick(NEXT);
        Thread.sleep(SLEEP_TIME);
        waitControlExists(TITLE_START_WINDOW,CHECK_BOX , SLEEP_TIME, SLEEP_TIME * 60);

        // Java - Window*
        controlClick(NEXT);
        waitControlExists(TITLE_START_WINDOW,EDIT_LICENSE,SLEEP_TIME, SLEEP_TIME * 5);

        //Agent - Window
        X.controlSend(TITLE_START_WINDOW, "", EDIT_LICENSE, AGENT_HOST_PORT);
        controlClick(NEXT);
        waitControlExists(TITLE_START_WINDOW,PASSWORD_EDIT_POSTGRES_CONFIRM,SLEEP_TIME,SLEEP_TIME * 5);


        // Parameter Postgres
        controlClick(NEXT);
        waitControlExists(TITLE_START_WINDOW,POSTGRES_PORT,SLEEP_TIME,SLEEP_TIME * 5);


        //Postgres Installation
        X.controlSend(TITLE_START_WINDOW, "", PASSWORD_EDIT_POSTGRES, PASSWORD);
        X.controlSend(TITLE_START_WINDOW, "", PASSWORD_EDIT_POSTGRES_CONFIRM, PASSWORD);
        controlClick(NEXT);
        waitControlExists(TITLE_START_WINDOW,FINAL_INSTALL_TEXT,SLEEP_TIME,SLEEP_TIME * 5);


        //Ready for Install
        controlClick(NEXT);
        Thread.sleep(SLEEP_TIME * 5);
        waitControlExists(TITLE_INSTALL_SQL,OK_BUTTON,SLEEP_TIME,SLEEP_TIME * 5);

        //Wait for Update
        X.winActivate(TITLE_INSTALL_SQL);
        X.winWaitActive(TITLE_INSTALL_SQL);
       // controlClick(OK_BUTTON);
        sendKey.keyPress(KeyEvent.VK_ENTER);
        waitControlExists(TITLE_START_WINDOW, NEXT, SLEEP_TIME, SLEEP_TIME * 600);

        controlClick(NEXT);

    }

    private void waitWindowExists(String window,int interval,int waitInterval) throws InterruptedException {
        int time=0;

        while (!X.winExists(window)) {
            Thread.sleep(interval);
            time+=interval;
            System.out.print("Not Exists");
        }
        System.out.print("Exists");
        if (time>waitInterval) {
            FTPSET5.setError("Ошибка! Дистрибутив не запустился! ");
            Assert.fail();
        }
    }

    private void waitControlExists(String window,String control,int interval,int waitInterval) throws  InterruptedException {
        int time = 0;

        while (!X.controlCommandIsVisible(window,"",control)) {
            Thread.sleep(interval);
            time += interval;
            System.out.print("Not Exists");
        }
        System.out.print("Exists");
        Thread.sleep(interval);

        if (time > waitInterval) {
            FTPSET5.setError("Ошибка! Дистрибутив не был установлен за указанное время! ");
            Assert.fail();
        }
      }


    private void controlClick(String control) throws  InterruptedException{
        Assert.assertEquals(X.controlClick(START_FORM, "", control), true);
        System.out.print("CLICK");
    }

    public void runClientInstall() throws IOException,InterruptedException,AWTException {
        String jacobDllVersionToUse = "jacob-1.18-x64.dll";
        File file = new File(jacobDllVersionToUse);

        System.setProperty("jna.library.path", file.getAbsolutePath());
        System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());
        Robot sendKey = new Robot();
        RunBatScript(START_DISTRIB, true);

        //License Window
        waitWindowExists(START_FORM,SLEEP_TIME,SLEEP_TIME*5);
        controlClick(RADIO_BUTTON_ACCEPT_LICENSE);

        controlClick(NEXT_START_WINDOW);
        waitControlExists(TITLE_START_WINDOW,EDIT_DIRECTORY, SLEEP_TIME, SLEEP_TIME * 5);


        //Directory window
        controlClick(NEXT);
        waitControlExists(TITLE_START_WINDOW,CHECK_BOX, SLEEP_TIME, SLEEP_TIME * 5);

        //client-server window

        X.controlGetFocus(TITLE_START_WINDOW);
        sendKey.keyPress(KeyEvent.VK_UP);
        sendKey.keyPress(KeyEvent.VK_SPACE);
        controlClick(NEXT);
        waitControlExists(TITLE_START_WINDOW,CHECK_BOX , SLEEP_TIME, SLEEP_TIME * 60);

        //Java-window
        controlClick(NEXT);
        waitControlExists(TITLE_START_WINDOW,PASSWORD_EDIT_POSTGRES,SLEEP_TIME,SLEEP_TIME * 5);

        X.controlSend(TITLE_START_WINDOW, "",EDIT_LICENSE, CLIENT_POSTGRES_IP);
        X.controlSend(TITLE_START_WINDOW, "", POSTGRES_PORT, PASSWORD);
        controlClick(NEXT);

        //Wait for update
        Thread.sleep(SLEEP_TIME*15);
        waitControlExists(TITLE_START_WINDOW, NEXT, SLEEP_TIME, SLEEP_TIME * 600);
        controlClick(NEXT);

        Thread.sleep(SLEEP_TIME);
    }


    private void RunBatScript(String name,Boolean ignore) throws IOException {

        System.out.print(name);

        ProcessBuilder builder = new ProcessBuilder(
                "cmd.exe", "/c", name);
        builder.redirectErrorStream(true);
        Process p = builder.start();

        if (!ignore) {
            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while (true) {
                line = r.readLine();
                if (line == null) {
                    break;
                }
                System.out.println(line);
            }
        }
    }
}
