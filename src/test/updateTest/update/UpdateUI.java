package test.updateTest.update;

import autoitx4java.AutoItX;
import com.jacob.com.LibraryLoader;
import org.testng.Assert;
import test.config.Log;
import test.config.Parameters;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;



/**
 * Created by d.fedorov on 22.01.2018.
 */
public class UpdateUI implements AutoCloseable  {

    private final String FILEPATH_64_VERSION = Parameters.system.get("ftp.toCopy") + Parameters.system.get("ftp.templatePath").replace("%version%", Parameters.system.get("VersionTo"))+"/Prisma.iss";
    private final String FILEPATH_32_VERSION = Parameters.system.get("ftp.toCopy") + Parameters.system.get("ftp.templatePath").replace("%version%", Parameters.system.get("VersionTo"))+"/Prisma_32bit.iss";
    private final String START_UPDATE = "cd " + Parameters.system.get("ftp.toCopy")+ Parameters.system.get("ftp.templatePath").replace("%version%", Parameters.system.get("VersionTo")) + "/Output" + " && " + Parameters.system.get("ftp.ResultName").replace("%version%", Parameters.system.get("VersionTo")).replace("(","^(") +"_64bit.exe"  +" /VERYSILENT";
    private final String  INNO_SETUP_LOCATION = "C:\\Program^ Files^ ^(x86)\\Inno^ Setup^ 5";
    private final AutoItX X = new AutoItX();
    private final String TITLE_SETUP = "Установка";
    private final String BUTTON_YES = "[CLASS:Button;INSTANCE:1]";
    private int PRE_UPDATE_TIME=300; //Time before confirmation box appears
    private int UPDATE_TIME=600;
    private String INNO_SETUP_PROCESS="Test";
    private final String PRISMA_EVENT_SERVER_LOG = Parameters.system.get("service.PrismaEventServerPath");
    private final String SET_PRISMA_LOG = Parameters.system.get("service.SetPrismaPath");
    private final String PRISMA_EVENT_SERVER_LINE= Parameters.system.get("service.PrismaEventServer");
    private final String SET_PRISMA_LINE= Parameters.system.get("service.SetPrisma");

    int time = 0;


    public void start() throws InterruptedException,IOException
   {
       //compile inno setup scripts

       String jacobDllVersionToUse = "jacob-1.18-x64.dll";
       File file = new File(jacobDllVersionToUse);

       RunBatScript(FILEPATH_32_VERSION,false);
       RunBatScript(FILEPATH_64_VERSION,false);

       Log.logger.debug(START_UPDATE);

       RunBatScript(START_UPDATE,true);


       System.setProperty("jna.library.path",file.getAbsolutePath());
       System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());

       while (time < PRE_UPDATE_TIME) {

           if (X.winExists(TITLE_SETUP)) {

               if (X.controlClick(TITLE_SETUP, "", BUTTON_YES)) {
                   Log.logger.debug("EXISTS");
                   break;
               }
           }
           time++;
           Thread.sleep(1000);
           Log.logger.debug("CALL" + X.winExists(TITLE_SETUP));
       }

       if (time >=300) {
           FTPSET5.setError("Ошибка! Не пройдены предварительные условия обновления!");
           Assert.fail();
       }

       while (time < UPDATE_TIME) {

           if (CheckProcessIsRunning(INNO_SETUP_PROCESS)) {
              time ++;
              Thread.sleep(5000);
           }

           else {
               break;
           }
       }

       if (time >=600) {
           FTPSET5.setError("Ошибка! Обновление не прошло за отведенное время!");
           Assert.fail();
       }


       if (!(checkService(PRISMA_EVENT_SERVER_LOG,PRISMA_EVENT_SERVER_LINE))) {
            FTPSET5.setError("Ошибка! Служба PrismaEventServer не стартовала!");
           Assert.fail();
       }
       if (!(checkService(SET_PRISMA_LOG,SET_PRISMA_LINE))) {
           FTPSET5.setError("Ошибка! Служба SetPrisma не стартовала!");
           Assert.fail();
       }
   }


    private boolean checkService(String log,String line) {

            boolean checkService = false;
            try {
                File file = new File(log);
                final Scanner scanner = new Scanner(file, "UTF-8");
                if (!file.exists()) {
                    Assert.assertEquals(true,false);
                }
                while (scanner.hasNext()) {
                    String lineFromFile = scanner.nextLine();
                    lineFromFile = lineFromFile.replaceAll("\u0000","");
                    if (lineFromFile.contains(line)) {
                        checkService = true;
                        break;
                    }
                }
            }

            catch (IOException e) {
                e.printStackTrace();
                FTPSET5.setError("Службы не стартовали корректно! В логе отсутсвуют записи о старте службы!");
                Assert.fail();
            }

            return  checkService;
    }

    private void RunBatScript(String name,Boolean ignore) throws IOException {
        String CompileUpdate="";

        if (name.equals(FILEPATH_32_VERSION)) {
            CompileUpdate = "cd " + INNO_SETUP_LOCATION + " && " + "iscc " + name + " /F" + Parameters.system.get("ftp.ResultName").replace("%version%", Parameters.system.get("VersionTo"))+"_32bit";
            System.out.print(CompileUpdate);
        }

        if (name.equals(FILEPATH_64_VERSION)) {
            CompileUpdate = "cd " + INNO_SETUP_LOCATION + " && " + "iscc " + name + " /F" + Parameters.system.get("ftp.ResultName").replace("%version%", Parameters.system.get("VersionTo"))+"_64bit";
            System.out.print(CompileUpdate);
        }

        if (name.equals(START_UPDATE)) {
            CompileUpdate = START_UPDATE;
        }

        ProcessBuilder builder = new ProcessBuilder(
                "cmd.exe", "/c", CompileUpdate);
        builder.redirectErrorStream(true);
        Process p = builder.start();

        if (!ignore) {
            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while (true) {
                line = r.readLine();
                if (line == null) {
                    break;
                }
                System.out.println(line);
            }
        }
    }

    private boolean CheckProcessIsRunning(String name) throws IOException {
        String line;
        String pidInfo ="";

        Process p =Runtime.getRuntime().exec(System.getenv("windir") +"\\system32\\"+"tasklist.exe");

        BufferedReader input =  new BufferedReader(new InputStreamReader(p.getInputStream()));

        while ((line = input.readLine()) != null) {
            pidInfo+=line;
        }

        input.close();

        return pidInfo.contains(name);
    }


    @Override
    public void close() {
        X.winKill(TITLE_SETUP);
    }
}
