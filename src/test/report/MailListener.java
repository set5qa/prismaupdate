package test.report;

import test.updateTest.update.FTPSET5;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.reporters.EmailableReporter2;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by d.fedorov on 10.05.2017.
 */

public class MailListener extends EmailableReporter2 implements ITestListener {

    private static void sendMail(int passed, int failed, int skipped, ArrayList <String> errors) {
        try {
            int all = 0;
            final String username = "autoqa@crystals.ru";
            final String password = "ca5JH4bymH";
            final String usernameto = "d.fedorov@crystals.ru";
            all = skipped + passed + failed;

            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {

                    @Override
                    protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                        return new javax.mail.PasswordAuthentication(username, password);
                    }
                });

            // Create a default MimeMessage object.

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(usernameto));
            if (passed != all) {
                message.setSubject("ERROR.Result Autotest.CI Prisma UPDATE");
            } else {
                message.setSubject("SUCCESS.Result Autotest.CI Prisma UPDATE");
            }


            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText("Все тесты завершены. \n" + "Количество тестов: \n" + "  Bceго: " + all + "\n" + "  Ошибочных: " + failed + "\n" +
                "  Успешных: " + passed + "\n" + "  Не пройденных: " + skipped + "\n");

            // Create a multipart message

            Multipart multipart = new MimeMultipart();

            for (String error:errors) {
                BodyPart messageBodyPart1 = new MimeBodyPart();
                messageBodyPart1.setText(error+ "\n");
                multipart.addBodyPart(messageBodyPart1);
            }

            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment

          //  messageBodyPart = new MimeBodyPart();
          //  DataSource source = new FileDataSource(filename);
         //   messageBodyPart.setDataHandler(new DataHandler(source));
         //   messageBodyPart.setFileName(filename);
         //   multipart.addBodyPart(messageBodyPart);

              message.setContent(multipart);
              Transport.send(message);

        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }


    @Override
    public void onStart(ITestContext arg0) {

    }

    @Override
    public void onTestStart(ITestResult arg0) {

    }

    @Override
    public void onTestSkipped(ITestResult arg0) {

    }

    @Override
    public void onTestFailure(ITestResult arg0) {
        sendMail(0,1,0, FTPSET5.getError());
    }

    @Override
    public void onTestSuccess(ITestResult arg0) {
        sendMail(1,0,0, FTPSET5.getError());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {

    }

    @Override
    public void onFinish(ITestContext arg0) {
        Reporter.log("Completed executing test " + arg0.getName(), true);
    }

}
